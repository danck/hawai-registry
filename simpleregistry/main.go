package simpleregistry

import (
	"flag"
	"github.com/gorilla/mux"
	"log"
	"net/http"
)

// Command line parameters
var (
	listenAddress = flag.String(
		"listen-addr",
		":32000",
		"Address to listen on")
	keepAlive = flag.Int(
		"time-to-live",
		3,
		"Seconds of service lifetime after last heartbeat")
	logFile = flag.String(
		"log-file",
		"hawai-simpleregistry.log",
		"Log file")
)

var (
	store *Store
)

func Main() {
	flag.Parse()

	// Set up key-value-store
	store = NewStore(*keepAlive)

	// Register handlers
	router := mux.NewRouter()
	router.HandleFunc("/service/{key:[0-9a-zA-Z]+}", errorHandler(serviceHandler))
	router.HandleFunc("/service", errorHandler(serviceHandler))
	router.HandleFunc("/subscribe", errorHandler(subscribeHandler))
	router.HandleFunc("/", errorHandler(statusHandler))

	// Start the server
	log.Printf("Starting to listen on %s", *listenAddress)
	log.Printf("Store status:\n%s", store.Status())
	log.Fatal(http.ListenAndServe(*listenAddress, router))
}
