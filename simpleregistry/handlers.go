package simpleregistry

import (
	"encoding/json"
	"errors"
	"github.com/gorilla/mux"
	"io/ioutil"
	"log"
	"net/http"
)

type Service struct {
	ID      string `json:"id"`
	Address string `json:"address"`
}

// serviceHandler adds new services to a to the registry
// If entries already exist, they are kept alive
func serviceHandler(w http.ResponseWriter, r *http.Request) error {
	if r.Method == "GET" {
		replyServiceList(w, r)
		return nil
	}

	if r.Method == "POST" {
		err := setOrKeepAliveEntry(w, r)
		return err
	}

	err := errors.New("Prefix exists, but no verb matched")
	return err
}

func statusHandler(w http.ResponseWriter, r *http.Request) error {
	if r.Method == "GET" {
		w.Write(store.Status())
		return nil
	}
	return errors.New("No matching HTTP verb")
}

// subscribeHandler adds new subscribers, that will get posted with
// the registry status when new events occur
func subscribeHandler(w http.ResponseWriter, r *http.Request) error {
	if r.Method == "POST" {
		err := addSubscriber(w, r)
		return err
	}
	return errors.New("Now verb hit")
}

func replyServiceList(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	key := vars["key"]
	entries := store.Get(key)
	response := map[string][]*ServiceType{"services": entries}
	responseJSON, _ := json.Marshal(response)
	w.Write(responseJSON)
}

func setOrKeepAliveEntry(w http.ResponseWriter, r *http.Request) error {
	vars := mux.Vars(r)
	key := vars["key"]
	if key == "" {
		return errors.New("No label-key given")
	}
	var s Service
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&s)
	if err != nil {
		return err
	}
	err = store.KeepAlive(key, s.ID)
	if err != nil {
		id := store.Set(key, s.Address)
		s.ID = id
		log.Println("New ID set %s", id)
	}
	resp, err := json.Marshal(s)
	if err != nil {
		return err
	}
	w.Write(resp)
	return nil
}

func addSubscriber(w http.ResponseWriter, r *http.Request) error {
	address, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return err
	}
	o := ObserverStruct{
		Address: string(address[:]),
		ID:      "not given",
	}
	store.AddObserver(&o)
	w.WriteHeader(200)
	return nil
}
