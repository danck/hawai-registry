package simpleregistry

import (
	"fmt"
	"log"
	"net/http"
)

func errorHandler(f func(w http.ResponseWriter, r *http.Request) error) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		err := f(w, r)
		if err != nil {
			log.Printf("ERROR: %s", err.Error())
			fmt.Fprintf(w, err.Error())
		}
		log.Printf("Store Status:%s\n", store.Status())
	}
}
