package simpleregistry

import (
	"bytes"
	"encoding/json"
	"errors"
	"log"
	"net/http"
	"strconv"
	"sync"
	"time"
)

//internal
type ServiceType struct {
	ID       int         `json:"id"`
	LastSeen time.Time   `json:"last-seen"`
	Address  string      `json:"address"`
	timer    *time.Timer // keep private, not json-marshalizable
}

type Observer interface {
	notify(s *Store)
}

type ObserverStruct struct {
	ID      string
	Address string
}

type Store struct {
	Mtx       *sync.Mutex
	LastID    int
	TTL       time.Duration
	Entries   map[string][]*ServiceType
	IDMap     map[string]string
	Observers []Observer
}

func NewStore(keepAlive int) *Store {
	return &Store{
		Mtx:       &sync.Mutex{},
		LastID:    0,
		TTL:       time.Duration(time.Second * time.Duration(keepAlive)),
		Entries:   make(map[string][]*ServiceType),
		IDMap:     make(map[string]string),
		Observers: make([]Observer, 8),
	}
}

// Set adds a service to the list under the given key
func (s *Store) Set(key string, value string) string {
	s.Mtx.Lock()
	defer s.Mtx.Unlock()
	defer s.notifyObservers()

	s.LastID += 1
	service := &ServiceType{
		ID:       s.LastID,
		LastSeen: time.Now(),
		Address:  value,
	}
	s.IDMap[strconv.Itoa(service.ID)] = key
	s.Entries[key] = append(s.Entries[key], service)

	service.timer = time.NewTimer(10 * time.Second)
	go func() {
		<-service.timer.C
		s.removeService(service)
	}()

	return strconv.Itoa(service.ID)
}

// Get returns the list of services for a given key
func (s *Store) Get(key string) []*ServiceType {
	s.Mtx.Lock()
	defer s.Mtx.Unlock()

	resultValues := make([]*ServiceType, 0)
	for _, entry := range s.Entries[key] {
		resultValues = append(resultValues, entry)
	}
	return resultValues
}

// KeepAlive resets the remaining validity duration to the store's default TTL
func (s *Store) KeepAlive(key string, idString string) error {
	s.Mtx.Lock()
	defer s.Mtx.Unlock()
	defer s.notifyObservers()

	if idString == "" {
		return errors.New("no id given")
	}

	//key, ok := s.IDMap[idString]
	//if !ok {
	//		return errors.New("Key not present")
	//	}

	id, err := strconv.Atoi(idString)
	if err != nil {
		return err
	}
	for _, servIter := range s.Entries[key] {
		if servIter.ID == id {
			servIter.timer.Reset(s.TTL)
			servIter.LastSeen = time.Now()
			return nil
		}
	}
	return errors.New("ServiceType not found")
}
func (s *Store) Status() []byte {
	s.Mtx.Lock()
	defer s.Mtx.Unlock()

	stats, err := json.Marshal(s)
	if err != nil {
		return []byte(err.Error())
	}
	return stats
}

// removeService removes a service from the registry
func (s *Store) removeService(service *ServiceType) {
	s.Mtx.Lock()
	defer s.Mtx.Unlock()
	defer s.notifyObservers()
	log.Println("Removing", service.ID, service.Address)

	key := s.IDMap[strconv.Itoa(service.ID)]
	var offset int
	for index, value := range s.Entries[key] {
		if value.ID == service.ID {
			offset = index
			break
		}
	}
	e := s.Entries[key]
	if offset < len(e)-1 {
		s.Entries[key] = append(e[:offset], e[offset+1:]...)
	} else {
		s.Entries[key] = e[:offset]
	}
	delete(s.IDMap, strconv.Itoa(service.ID))
}

func (s *Store) AddObserver(o Observer) {
	s.Observers = append(s.Observers, o)
}

//TODO(danck): implement, test
//func (s *Store) RemoveObserver(o Observer) {
//}

func (s *Store) notifyObservers() {
	if s == nil {
		log.Println("store is nil")
	}
	if len(s.Observers) == 0 {
		log.Println("no observers registered")
		return
	}
	for _, obs := range s.Observers {
		if obs != nil {
			obs.notify(s)
		}
	}
}

func (o *ObserverStruct) notify(s *Store) {
	go func() {
		stats := s.Status()
		http.Post(o.Address, "application/text", bytes.NewReader(stats))
	}()
}
