package simpleregistry

import (
	"testing"
)

func TestGetSingleValue(t *testing.T) {
	store := NewStore(12)

	key := "hawai-crm"
	value := []byte("localhost:8080")
	expect := `["localhost:8080"]`

	store.Set(key, value)
	result := store.Get(key)
	if result != expect {
		t.Errorf("Expected %s, but got %s", expect, result)
	}
}
