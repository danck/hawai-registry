FROM golang

ADD . /go/src/gitlab.com/danck/hawai-simpleregistry

RUN go get "github.com/gorilla/mux"
RUN go install "github.com/gorilla/mux"
RUN go install gitlab.com/danck/hawai-simpleregistry

ENV HOST_IP=192.168.29.143
ENV HOST_PORT=32000

ENTRYPOINT /go/bin/hawai-simpleregistry 

EXPOSE 32000

