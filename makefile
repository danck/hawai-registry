MAKEFLAGS += --warn-undefined-variables
SHELL := /bin/bash
.DEFAULT_GOAL := build

.PHONY: clean lint

ROOT := $(shell pwd)
PACKAGE := HAWAI/repos/hawai-simpleregistry

clean:
	rm -rf build cover
	rm hawai-simpleregistry

build:
	go build -v

rebuild: clean build

test:
	go test -v -race ./...

lint:
	go vet ./...
	golint ./...

package:
	sudo docker build -t hawai-simpleregistry $(ROOT)

all: clean build lint test package

deploy:
	sudo docker run --publish 32000:32000 --name test-registry --rm hawai-simpleregistry

run:
	    sudo docker run --publish 32000:32000 --name registry --rm hawai-simpleregistry
